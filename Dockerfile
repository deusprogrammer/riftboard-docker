FROM tomcat:8-jre8
MAINTAINER "Michael C. Main deusprogrammer@gmail.com"

ADD riftboard.war /usr/local/tomcat/webapps/
ADD manager.xml /usr/local/tomcat/conf/Catalina/localhost/
ADD settings.xml /usr/local/tomcat/conf/
ADD tomcat-users.xml /usr/local/tomcat/conf/
